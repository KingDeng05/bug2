import RPi.GPIO as GPIO
import time
import math as m
import numpy as np

#-----------------GPIO SETUP-----------------------#
GPIO.setmode(GPIO.BOARD)
#SET THE LEFT WHEEL
GPIO.setup(13,GPIO.OUT) #STEP
GPIO.setup(7,GPIO.OUT)  #DIRECTION
#SET THE RIGHT WHEEL
GPIO.setup(12,GPIO.OUT) #STEP
GPIO.setup(8,GPIO.OUT)  #DIRECTIOIN
#SET THE FRONT SENSOR
GPIO.setup(16,GPIO.IN)
#SET THE RIGHT SENSOR
GPIO.setup(18,GPIO.IN)

#----------------DATA SETUP-------------------------#
#ROBOT PARAMETER
wheel_dist = 72       #NEED CALIBRATION AGAIN       
#STEP MOTOR PARAMETER
dist_per_tick = 0.46
num_tick_per_rad = 475/(m.pi)
delay_time = 0.005
sleep_time=0.005
step = 55    #HOW MANY COUNTS IN ONE STEP
#TOLERANCE
tol_dist = 180         #TOLERANCE OF DISTANCE (RADIUS OF CIRCLE)
err = 1e-5             #TOLERANCE FOR COMPUTATION ERROR
#STATIC OR GLOBAL PARAMETER
tf_l = False
tf_r = False

#---------------FUNCTION SETUP-----------------------%
#REVERSe TRUE AND FALSE
def switch_tf(tf):
	if tf == True:
		tf = False
	else:
		tf = True
	return tf
#CALCULATE CURRENT POSITION AFTER TURNING, FIRST TRIAL: 90 DEGREE!
def update_pos_turn(angle):
	global A_cur
	global x_cur
	global y_cur
	global num_tick_per_rad
	A_pre = A_cur
	A_cur = A_pre + angle
	if abs(A_cur-2*m.pi)<err or abs(A_cur+2*m.pi)<err:
		A_cur = 0
	a = wheel_dist/2*m.sin(angle)
	b = wheel_dist/2*(1-m.cos(angle))
	if angle > 0:
		if A_cur > 0 and abs(A_pre-0)<err:
			x_cur = x_cur+a
			y_cur = y_cur+b
		elif abs(A_cur+m.pi)<err and abs(A_pre+-m.pi/2)<err:
			x_cur = x_cur-b
			y_cur = y_cur+a
		elif abs(A_cur+3*m.pi/2)<err and abs(A_pre+m.pi)<err:
			x_cur = x_cur-a
			y_cur = y_cur-b
		else:
			x_cur = x_cur+b
			y_cur = y_cur-a
	else:
		if abs(A_cur+m.pi/2)<err and abs(A_pre)<err:
			x_cur = x_cur-a
			y_cur = y_cur-b
		elif abs(A_cur+m.pi)<err and abs(A_pre+m.pi/2)<err:
			x_cur = x_cur-b
			y_cur = y_cur+a
		elif abs(A_cur+3*m.pi/2)<err and abs(A_pre+m.pi)<err:
			x_cur = x_cur+a
			y_cur = y_cur+b
		else:
			x_cur = x_cur+b
			y_cur = y_cur-a
	#num_tick_per_rad = num_tick_per_rad + 2/(m.pi)
	print "My position right now is %f, %f, %f" %(x_cur,y_cur,A_cur)	

#CALCULATE CURRENT POSITION AFTER MOVING FORWARD
def update_pos_move(count):
	global x_cur
	global y_cur
	if abs(A_cur-m.pi/2)<err:
		y_cur = y_cur+dist_per_tick*count
	elif abs(A_cur)<err:
		x_cur = x_cur+dist_per_tick*count
	elif abs(A_cur+m.pi/2)<err:
		y_cur = y_cur-dist_per_tick*count
	else:
		x_cur = x_cur+dist_per_tick*count	
	print "My position right now is %f, %f, %f" %(x_cur,y_cur,A_cur)
	
#TURNING FUNCTION,lor MEANS 1-LEFT OR 2-RIGHT
def turn(angle_rad):
	print "I'm turning %f degree"	%angle_rad
	global tf_r
	global tf_l
	if angle_rad > 0:
		pin = 12
		tf = tf_r
	else:
		pin = 13
		tf = tf_l 
	total_tick = round(abs(angle_rad)*num_tick_per_rad)
	count = 0
	while count < total_tick:
		tf = switch_tf(tf)
		GPIO.output(pin,tf)
		time.sleep(delay_time)
		count = count+1
	if angle_rad > 0:
		tf_r = tf
	else:
		tf_l = tf
	time.sleep(sleep_time)
	update_pos_turn(angle_rad)

def move_forward(a):
	print "I'm moving forward now!"
	global tf_r
	global tf_l
	count = 0
	while count <= a :
		tf_r = switch_tf(tf_r)
		tf_l = switch_tf(tf_l)
		GPIO.output(13,tf_l)
		GPIO.output(12,tf_r)
		time.sleep(delay_time)
		count = count+1
	time.sleep(sleep_time)
	update_pos_move(count)

def move_to_goal():
	move_forward(step)
	print "I'm moving towards the goal!"

def follow_wall():
	while True:
		if GPIO.input(16) == True and GPIO.input(18) == True:
			move_forward(step)
			if y_cur > -15 and y_cur < 15:
				break
			move_forward(step)
			if y_cur > -15 and y_cur < 15:
				break
			move_forward(step)
			if y_cur > -15 and y_cur < 15:
				break
			turn(-m.pi/2)
			move_forward(1.7*step)
		elif GPIO.input(16) == False:
			turn(m.pi/2)
			time.sleep(0.5)
			continue
		else:
			move_forward(round(1.5*step))
		print "I'm following the wall!"
		print "y_cur now is %f" %y_cur
		if y_cur > -15 and y_cur < 15:
			break 
def arrive():
	dist = np.sqrt((x_goal-x_cur)**2+(y_goal-y_cur)**2)
	if dist <= tol_dist:
		tf = True
	else:
		tf = False
	print "The distance right now is %f" %dist
	return tf

#-------------PROGRAM START---------------------------%		
#INITIAL LOCATION
x_cur = 0    
y_cur = 0
A_cur = 0      #A IS NOTATION FOR ANGLE

#GOAL LOCATION
x_goal = 1500   #IN MILLIMETER
y_goal = 0

#ADJUST DIRECTION BEFORE MOVING TO GOAL
x_relat = np.array([x_goal - x_cur])
y_relat = np.array([y_goal - y_cur])
A_relat = np.arctan2(y_relat,x_relat)
A_turn = A_relat[0] - A_cur
turn(A_turn)

#RESET START POINT AND GOAL POINT
x_goal = np.sqrt((x_cur-x_goal)**2 + (y_cur-y_goal)**2)
y_goal = 0
A_cur = 0
x_cur = 0
y_cur = 0

#SET WHEEL DIRECTION
GPIO.output(7,False)
GPIO.output(8,True)

#SLEEP A WHILE TO START
time.sleep(1)
print "PLEASE HOLD YOUR BREATH..."

#START NAVIGATING HERE!
try:
	while arrive() == False:

		num_tick_per_rad = 480/(m.pi)	
		#MOVE TOWARDS THE GOAL
		move_to_goal()

		#IF SENSING OBSTABLE
		if GPIO.input(16) == False:
			#TURN LEFT
			turn(m.pi/2)
			follow_wall()
			print "I finished following the wall!"
			#TURN LEFT
			num_tick_per_rad = 490/(m.pi)
			turn(m.pi/2)	
	GPIO.cleanup()				 
except KeyboardInterrupt:
	print "interrupted!"
	GPIO.cleanup()

		

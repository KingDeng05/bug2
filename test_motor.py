import RPi.GPIO as gpio
import time

gpio.setmode(gpio.BOARD)
#SET UP THE RIGHT WHEEL
gpio.setup(12, gpio.OUT)
gpio.setup(8, gpio.OUT)
#SET UP THE LEFT WHEEL
gpio.setup(13, gpio.OUT)
gpio.setup(7,gpio.OUT)
#CHANGE WHEEL DIRECTION
gpio.output(7,False)
gpio.output(8,True)
count = 0
tf = False
try:
	while True:
		#turning direction, not neccesary
		#if count ==800:
			#gpio.output(8,tf)
			#gpio.output(7,tf)
			#if tf == True:
				#tf = False
			#else:
				#tf = True
			#count = 0
		#sensing every tick
		#if gpio.input(13) == False:
			#break
		#move forward
		if tf == False:
			tf = True
		else:
			tf = False
		gpio.output(12,tf)
		gpio.output(13,tf)
		time.sleep(0.01)
		count = count + 1
except KeyboardInterrupt:
	print 'interrupted!'
	print 'The total tick is %d' %count
	gpio.cleanup()



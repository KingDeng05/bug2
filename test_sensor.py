import RPi.GPIO as gpio
import time

gpio.setmode(gpio.BOARD);

gpio.setup(18,gpio.IN);

count = 1;
try:
	while True:
		if gpio.input(18) == False:
			print "Object detected %d" %count
			count = count + 1;
except KeyboardInterrupt:
	print "Interrupted!";
	gpio.cleanup();

